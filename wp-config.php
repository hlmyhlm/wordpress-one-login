<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpressuser');

/** MySQL database password */
define('DB_PASSWORD', 'password');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'L+.L7{Q lNBM D.9=#sTere}e{K1sY.Z`)F;-n7DVmO@qIJX-|M38]q*{HR)UTBe');
define('SECURE_AUTH_KEY',  'FcYV#2F-smtn+V8}hu~|Q3Thqv66<~/FQD@@EE*Ay/q<9Vy|}t<-x:bMkjq}JPVz');
define('LOGGED_IN_KEY',    'GP%v^j/7+mbfoRe/ZJY!|en:s>AZ-53* $I)+3+#1;(>r(5YwFaKrP5-#_`|R)QD');
define('NONCE_KEY',        'eA(;l`xG3+1OzE<#$-+tpy/-}KW14=)ab)8veaMc,-zy|omZe9ucVg5cem<_*|Pn');
define('AUTH_SALT',        ']@|b[g8C?^8@JMHr_hm|*}+JK-gH/I34N+pIpvGS>TZLUI!0|-M&~4p&z=pz{NZ@');
define('SECURE_AUTH_SALT', '~d2Bi]=lIp#tkjv:%M5Daw^bUL>ax,@$3v&~UcyjDJUz`:+4$!zqlfCcrvl5&h)-');
define('LOGGED_IN_SALT',   'iYR.AWvexDk>+<WX%-^{kq[q_60~m`~N,rO4g$?d^y:NHW(QEu7[~tDP%>[TC~TA');
define('NONCE_SALT',       '|[?wm]v|vh%W4pyO+-:ltmu}FQlE/E7uFw%vm-Pzuv1ev7T-wT%NO5o:p0XW^Pf9');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
